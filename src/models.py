# Copyright (C) 2019-2020 VGR SpA - All Rights Reserved
# Copyright (C) 2019-2020 Ingenieria y Energia NegaWatt Ltda. - All Rights Reserved
#
# Unauthorized copying of this file, via any medium is strictly prohibited.
# This code is proprietary and confidential.
# You may not use this file except in compliance with the Copyright holders.
# Any kind of misuse of this file and intellectual properties may incur in legal prosecution.
#
# Author: Victor Gonzalez <victor@vgr.cl>

"""SQLite Models Interface

This scripts holds all the logic that handles the local database interactions.
Also, this script will create the database file if possible.
"""

import os

from peewee import SqliteDatabase, Model, CharField, BooleanField, TextField, IntegerField, AutoField

db = SqliteDatabase('local.db')


class BaseModel(Model):
    class Meta:
        database = db

class Measurement(BaseModel):
    id = AutoField()
    chilemedido_device_number = IntegerField()
    data = TextField()
    local_timestamp = IntegerField()
    sent = BooleanField(default=False)


class Event(BaseModel):
    id = AutoField()
    device_id = IntegerField()
    type = CharField()
    data = TextField()
    timestamp = IntegerField()
    sent = BooleanField(default=False)


if not os.path.isfile('local.db'):
    db.create_tables([Measurement])
    print('Database initiated...')
elif not db.table_exists('event'):
    # add new table
    db.create_tables([Event])
    print('Added event table...')
