# Copyright (C) 2019-2020 VGR SpA - All Rights Reserved
# Copyright (C) 2019-2020 Ingenieria y Energia NegaWatt Ltda. - All Rights Reserved
#
# Unauthorized copying of this file, via any medium is strictly prohibited.
# This code is proprietary and confidential.
# You may not use this file except in compliance with the Copyright holders.
# Any kind of misuse of this file and intellectual properties may incur in legal prosecution.
#
# Author: Victor Gonzalez <victor@vgr.cl>

"""General Purpose Utils

This script holds several functions and utilities that are commonly used across the application.
"""

import json
import math
import subprocess
import time

from src.models import Measurement, Event


def format_power_factor(value):
    """Formats the power factor according to the Schneider documentation.

    :param value: The power factor value as read from the registers
    :type value: float
    :return: The formatted power factor
    :rtype: float
    """
    if -2 < value <= -1:
        return 2 + value
    elif -1 < value <= 1:
        return abs(value)
    elif 1 < value < 2:
        return 2 - value


def data_to_chilemedido(measurement):
    """Formats a database entry to the Chilemedido format.

    :param measurement: the database measurement
    :type measurement: array
    :return: the measurment in Chilemedido format
    :rtype: array
    """
    measure_data = json.loads(measurement.data)
    formatted_data = list()

    for k, v in measure_data.items():
        if not math.isnan(v):
            if k == 'Factor de potencia total':
                v = format_power_factor(v)
            formatted_data.append({'sensorName': k, 'sensorValue': v})
        else:
            formatted_data.append({'sensorName': k, 'sensorValue': 0})

    return [{
        'deviceNumber': measurement.chilemedido_device_number,
        'measures': [{
            'timestamp': measurement.local_timestamp,
            'sensorValues': formatted_data
        }]
    }]


def get_temp():
    """Obtains the temperature of the device.

    :return: temperature
    :rtype: float
    """
    result = subprocess.run(['cat', '/sys/class/thermal/thermal_zone0/temp'], stdout=subprocess.PIPE).stdout.decode(
        'utf-8')
    return int(result) / 1000.0


def data_sent_event(device_id, data=None):
    """Creates a ``data-sent`` event for the device ``device_id``

    :param device_id: the device ID
    :type device_id: integer
    :param data: additional data, defaults to None
    :type data: array, optional
    """
    Event.create(
        device_id=device_id,
        type='data-sent',
        timestamp=int(time.time()),
        data=json.dumps(data)
    )


def historical_data_sent_event(device_id, data=None):
    """Creates a ``historical-data-sent`` event for the device ``device_id``

    :param device_id: the device ID
    :type device_id: integer
    :param data: additional data, defaults to None
    :type data: array, optional
    """
    Event.create(
        device_id=device_id,
        type='historical-data-sent',
        timestamp=int(time.time()),
        data=json.dumps(data)
    )


def comx_error_event(device_id, data=None):
    """Creates a ``comx-error`` event for the device ``device_id``

    :param device_id: the device ID
    :type device_id: integer
    :param data: additional data, defaults to None
    :type data: array, optional
    """
    Event.create(
        device_id=device_id,
        type='comx-error',
        timestamp=int(time.time()),
        data=json.dumps(data)
    )


def chilemedido_error_event(device_id, data=None):
    """Creates a ``chilemedido-error`` event for the device ``device_id``

    :param device_id: the device ID
    :type device_id: integer
    :param data: additional data, defaults to None
    :type data: array, optional
    """
    Event.create(
        device_id=device_id,
        type='chilemedido-error',
        timestamp=int(time.time()),
        data=json.dumps(data)
    )


def heartbeat_event(device_id):
    """Creates a ``heartbeat`` event for the device ``device_id``

    This particular function will send useful data from the device, such as temperature and queue size.

    :param device_id: the device ID
    :type device_id: integer
    """
    Event.create(
        device_id=device_id,
        type='heartbeat',
        timestamp=int(time.time()),
        data=json.dumps({
            'temperature': get_temp(),
            'queue_size': Measurement.select().where(Measurement.sent == False).count()
        })
    )
