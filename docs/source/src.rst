Utils and Shared Business Logic
===============================

ChileMedido API Client
----------------------

.. automodule:: src.api_client
   :members:
   :undoc-members:
   :show-inheritance:

Config File Utils
-----------------

.. automodule:: src.config
   :members:
   :undoc-members:
   :show-inheritance:

Modbus Data Reader
------------------

.. automodule:: src.modbus_reader
   :members:
   :undoc-members:
   :show-inheritance:

Modbus Registers Utils
----------------------

.. automodule:: src.register_convert
   :members:
   :undoc-members:
   :show-inheritance:

SQLite Models Interface
-----------------------

.. automodule:: src.models
   :members:
   :undoc-members:
   :show-inheritance:

Monitoring API Client
-----------------------------

.. automodule:: src.monitoring_client
   :members:
   :undoc-members:
   :show-inheritance:


General Purpose Utils
----------------

.. automodule:: src.utils
   :members:
   :undoc-members:
   :show-inheritance:
