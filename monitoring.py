# Copyright (C) 2019-2020 VGR SpA - All Rights Reserved
# Copyright (C) 2019-2020 Ingenieria y Energia NegaWatt Ltda. - All Rights Reserved
#
# Unauthorized copying of this file, via any medium is strictly prohibited.
# This code is proprietary and confidential.
# You may not use this file except in compliance with the Copyright holders.
# Any kind of misuse of this file and intellectual properties may incur in legal prosecution.
#
# Author: Victor Gonzalez <victor@vgr.cl>

"""Sends telemetry and log data to Negawatt support platform

This script will read data from the local database, and send the data to Negawatt platform. By default,
this script will pick the last 50 events that are marked as not sent.

If the data is correctly sent, the data will be marked as sent,
if not, it will remain as not sent and will be picked up later by this same process.
"""

import sys

from src.config import load_config
from src.models import Event
from src.monitoring_client import MonitoringClient, AuthenticationError, ApiError


def send_monitoring_data():
    """Performs the script steps.
    """
    config = load_config()  # load configuration

    query = Event.select() \
        .where(Event.sent == False) \
        .order_by(Event.timestamp.asc()) \
        .limit(50)

    client = MonitoringClient(config['monitor']['access_token'])

    for event in query:
        try:
            response = client.send(event.device_id, event.type, event.timestamp, event.data)
            if response.json() and response.json()["id"]:
                event.sent = True
                event.save()
                print("Sent event id %s for device number %s" % (event.id, event.device_id))
        except Event.DoesNotExist:
            print("No events")
            pass
        except AuthenticationError as err:
            print(err)
            pass
        except ApiError as err:
            print("Monitoring API Error: %s" % err)
            pass
        except Exception as err:
            print("Unknown error: %s" % err)
            pass


if __name__ == "__main__":
    sys.exit(send_monitoring_data())
