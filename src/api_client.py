# Copyright (C) 2019-2020 VGR SpA - All Rights Reserved
# Copyright (C) 2019-2020 Ingenieria y Energia NegaWatt Ltda. - All Rights Reserved
#
# Unauthorized copying of this file, via any medium is strictly prohibited.
# This code is proprietary and confidential.
# You may not use this file except in compliance with the Copyright holders.
# Any kind of misuse of this file and intellectual properties may incur in legal prosecution.
#
# Author: Victor Gonzalez <victor@vgr.cl>

"""A simple API client to send information to ChileMedido REST API.


Example usage:

.. highlight:: python
.. code-block:: python

    from api_client import ApiClient

    access_token = some_access_token
    refresh_token = some_refresh_token
    client = ApiClient(access_token, refresh_token)
    print(client.get_devices_id(1).json())

CLI interface provided via the `fire`_ package. The same code as the above via CLI:

.. highlight:: bash
.. code-block:: bash

    python api_client.py --access-token=some_access_token --refresh_token=some_refresh_token get_devices_id 1

.. _fire: https://github.com/google/python-fire
"""

import json

import fire
import requests


class ApiError(Exception):
    """Handles general API errors.
    """
    pass


class AuthenticationError(Exception):
    """Handles authentication errors.
    """
    pass


class ApiClient(object):
    """A straightforward implementation of an API client for ChileMedido.

    This class handles all the basic logic for handling connections to the ChileMedido API.
    All the functions will return a request response object, not just a text.
    When an error occurs, an appropriate error will be raised, so remember to handle those exceptions.

    :param access_token: The ChileMedido API access token.
    :type access_token: string
    :param refresh_token: The ChileMedido API refresh token.
    :type refresh_token: string
    :raises AuthenticationError: Triggers when the authentication process cannot be completed.
    :raises ApiError: Triggers when a response is different from the 200 status code.
    """

    def __init__(self, access_token, refresh_token, verbose=False):
        self.access_token = access_token
        self.refresh_token = refresh_token
        self.verboseprint = print if verbose else lambda *a, **k: None

    def _url(self, path):
        """Formats the ChileMedido API url paths.

        :param path: An API path in the format '/this/path'.
        :type path: string
        :return: An absolute URL to the ChileMedido API
        :rtype: string
        """
        return 'https://in.chilemedido.cl/api/v1' + path

    def _header(self):
        """Handles the ChileMedido API header requests.

        :return: The request header with authorization tokens.
        :rtype: dictionary
        """
        return {
            'Authorization': 'Bearer ' + self.access_token,
            'User-Agent': 'NegawattApiClient/2020.03.05'
        }

    def handle_status(self, response):
        """Handles the ChileMedido status codes in the responses.

        :param response: A request response object.
        :type response: class:`requests.Response`
        :raises AuthenticationError: Triggers when the authentication process cannot be completed.
        :raises ApiError: Triggers when a response is different from the 200 status code.
        :return: The original response object
        :rtype: class:`requests.Response`
        """
        if response.status_code == 401:
            raise AuthenticationError('Error 401: Credentials are not valid.')
        elif response.status_code == 400:
            error_message = 'Error 400: Formatting error.'

            try:
                data = response.json()
                errors = "\n\t - ".join(data['errors'])
                error_message += "\n\t - %s" % (errors)
            except:
                pass

            raise ApiError(error_message)
        elif response.status_code != 200 and response.status_code != 201:
            try:
                data = response.json()
                error = data['error']
                description = data['error_description']
                raise ApiError('Error %s: %s' % (error, description))
            except:
                raise ApiError('Error %s while trying to access ChileMedido API.' % (response.status_code))

        return response

    def renew_credentials(self):
        """Performs the token refresh procedure on the ChileMedido API.

        Please note that this function will store the new credentials within this object, and you must make sure to
        store the refresh and access tokens in a permanent storage solution.

        :raises AuthenticationError:  Triggers when the authentication process cannot be completed.
        :return: The request response object.
        :rtype: class:`requests.Response`
        """
        response = requests.post(self._url('/auth/token/refresh'), json={
            'refreshToken': self.refresh_token
        })

        try:
            data = response.json()
            self.access_token = data['accessToken']
            self.refresh_token = data['refreshToken']
            self.verboseprint('Access token: %s' % self.access_token)
            self.verboseprint('Refresh token: %s' % self.refresh_token)
        except json.decoder.JSONDecodeError:
            raise AuthenticationError('Error while retrieving new tokens: invalid response.')
        except:
            raise ApiError('Error while retrieving new tokens: Error %s' % response.status_code)
        finally:
            return response

    def get_devices_id(self, id):
        """Performs the GET /devices/{id} request on the API.

        :param id: The device id.
        :type id: integer, string
        :return: The request response object.
        :rtype: class:`requests.Response`
        """
        return self.handle_status(requests.get(self._url('/devices/%s' % (id)), headers=self._header()))

    def get_devices_number(self, number):
        """Performs the GET /devices//number/{number} request on the API.

        :param number: The device number.
        :type number: integer, string
        :return: The request response object.
        :rtype: class:`requests.Response`
        """
        return self.handle_status(requests.get(self._url('/devices/number/%s' % number), headers=self._header()))

    def add_measurement(self, measurement):
        """Performs the POST /measurements request on the API.

        The object passed to this function must have the following structure: ::

          [
            {
              "deviceId": 1,
              "measures": [
                {
                  "timestamp": 1566226133,
                  "sensorValues": [
                    {
                      "sensorName": "sensor1",
                      "sensorValue": 3.7
                    }
                  ]
                }
              ]
            }
          ]

        :param measurement: A measurement object. This will be converted to a JSON object.
        :type measurement: array, dictionary
        :return: The request response object.
        :rtype: class:`requests.Response`
        """
        return self.handle_status(requests.post(self._url('/measurements'), headers=self._header(), json=measurement))

    def get_server_time(self):
        """Performs the GET /server-time request on the API.

        :return: The request response object.
        :rtype: class:`requests.Response`
        """
        return self.handle_status(requests.get(self._url('/server-time'), headers=self._header()))

    def get_sensor_name_list(self):
        """Performs the GET /sensor-name-list request on the API.

        :return: The request response object.
        :rtype: class:`requests.Response`
        """
        return self.handle_status(requests.get(self._url('/sensor-name-list'), headers=self._header()))

    def get_spec(self):
        """Performs the GET /spec request on the API.

        :return: The request response object.
        :rtype: class:`requests.Response`
        """
        return self.handle_status(requests.get(self._url('/spec'), headers=self._header()))


if __name__ == '__main__':
    fire.Fire(ApiClient)
