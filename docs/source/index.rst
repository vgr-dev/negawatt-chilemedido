.. Negawatt Chilemedido documentation master file, created by
   sphinx-quickstart on Fri May 22 18:27:55 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Negawatt ChileMedido Documentation
================================================

This is documentation is written and built for developers, and must be considered
the official development documents.

For non-developers, please contact your project manager and ask for the simplified
documentation for this project.

Source code is availablein Gitlab under the name of `negawatt-chilemedido`_ (private repo), and public binaries are available under
the name of `negawatt-chilemedido-public`_.

.. _negawatt-chilemedido: https://gitlab.com/vgr-dev/negawatt-chilemedido
.. _negawatt-chilemedido-public: https://gitlab.com/vgr-dev/negawatt-chilemedido-public

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   fetch_measurement
   send_data
   send_historical_data
   monitoring
   src

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
