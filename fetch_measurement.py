# Copyright (C) 2019-2020 VGR SpA - All Rights Reserved
# Copyright (C) 2019-2020 Ingenieria y Energia NegaWatt Ltda. - All Rights Reserved
#
# Unauthorized copying of this file, via any medium is strictly prohibited.
# This code is proprietary and confidential.
# You may not use this file except in compliance with the Copyright holders.
# Any kind of misuse of this file and intellectual properties may incur in legal prosecution.
#
# Author: Victor Gonzalez <victor@vgr.cl>

"""Reads registers from a modbus device and stores the formatted data in a local database.

This script will read data from a modbus device via TCP, read some specific registers defined
by the ``device_mapping`` folder files, depending on the configuration of the device.

The it will store the data in the local database and log data too.

Currently only supports Schneider PM3250 and iEM2150 monitors.
"""

import json
import sys
import time

from pymodbus.exceptions import ConnectionException

from src.config import load_config
from src.modbus_reader import ModbusReader
from src.models import Measurement
from src.utils import comx_error_event

def fetch_measurement():
    """Performs the script steps.
    """
    config = load_config()  # load configuration

    for device in config['devices']:
        # iterate over each device configuration
        device_config = config['devices'][device]

        # setup the modbus reader
        reader = ModbusReader(
            address=config['modbus']['ip'],
            port=config['modbus']['port'],
            slave=device_config['slave'],
            register_offset=device_config['register_offset'])

        # perform the measurment and save it to the database
        try:
            if device_config['type'] == 'pm3250':
                measure = reader.measure_pm3250()
                Measurement.create(
                    chilemedido_device_number=device_config['chilemedido_number'],
                    data=json.dumps(measure),
                    local_timestamp=int(time.time()),
                    sent=False
                )
            elif device_config['type'] == 'iem2150':
                measure = reader.measure_iem2150()
                Measurement.create(
                    chilemedido_device_number=device_config['chilemedido_number'],
                    data=json.dumps(measure),
                    local_timestamp=int(time.time()),
                    sent=False
                )
        except ConnectionException as err:
            comx_error_event(device_config['chilemedido_number'], {
                'message': str(err),
                'slave': device_config['slave']
            })
            print(err)
            pass
        except Exception as err:
            comx_error_event(device_config['chilemedido_number'], {
                'message': str(err),
                'slave': device_config['slave']
            })
            print(err)
            pass

    return 0


if __name__ == "__main__":
    sys.exit(fetch_measurement())
