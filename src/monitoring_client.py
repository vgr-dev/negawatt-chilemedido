# Copyright (C) 2019-2020 VGR SpA - All Rights Reserved
# Copyright (C) 2019-2020 Ingenieria y Energia NegaWatt Ltda. - All Rights Reserved
#
# Unauthorized copying of this file, via any medium is strictly prohibited.
# This code is proprietary and confidential.
# You may not use this file except in compliance with the Copyright holders.
# Any kind of misuse of this file and intellectual properties may incur in legal prosecution.
#
# Author: Victor Gonzalez <victor@vgr.cl>

"""A simple API client to send information to Negawatt Monitoring REST API

CLI interface provided via the `fire`_ package.

.. _fire: https://github.com/google/python-fire
"""

import json

import fire
import requests


class ApiError(Exception):
    """Handles general API errors.
    """
    pass


class AuthenticationError(Exception):
    """Handles authentication errors.
    """
    pass


class MonitoringClient(object):
    """A straightforward implementation of an API client for Negawatt Monitoring

    This class handles all the basic logic for handling connections to the Negawatt Monitoring API.
    All the functions will return a request response object, not just a text.
    When an error ocurs, an appropiate error will be raised, so remember to handle those exceptions.

    :param access_token: The Negawatt Monitoring API access token.
    :type access_token: string
    :raises AuthenticationError: Triggers when the authentication process cannot be completed.
    :raises ApiError: Triggers when a response is different from the 200 status code.
    """

    def __init__(self, access_token, verbose=False):
        self.access_token = access_token
        self.verboseprint = print if verbose else lambda *a, **k: None

    def _url(self, path):
        """Formats the Negawatt Monitoring API url paths.

        :param path: An API path in the format '/this/path'.
        :type path: string
        :return: An absolute URL to the Negawatt Monitoring API
        :rtype: string
        """
        return 'https://api-chilemedido.vgr.cl/v1' + path

    def _header(self):
        """Handles the Negawatt Monitoring API header requests.

        :return: The request header with authorization tokens.
        :rtype: dictionary
        """
        return {
            'Authorization': 'Bearer ' + self.access_token,
            'User-Agent': 'NegawattMonitoringApiClient/2020.04.27'
        }

    def handle_status(self, response):
        """Handles the Negawatt Monitoring status codes in the responses.

        :param response: A request response object.
        :type response: class:`requests.Response`
        :raises AuthenticationError: Triggers when the authentication process cannot be completed.
        :raises ApiError: Triggers when a response is different from the 200 status code.
        :return: The original response object
        :rtype: class:`requests.Response`
        """
        if response.status_code == 401:
            raise AuthenticationError('Error 401: Credentials are not valid.')
        elif response.status_code == 400:
            error_message = 'Error 400: Formatting error.'

            try:
                data = response.json()
                errors = "\n\t - ".join(data['errors'])
                error_message += "\n\t - %s" % (errors)
            except:
                pass

            raise ApiError(error_message)
        elif response.status_code != 200 and response.status_code != 201:
            try:
                data = response.json()
                error = data['error']
                description = data['error_description']
                raise ApiError('Error %s: %s' % (error, description))
            except:
                raise ApiError('Error %s while trying to access Negawatt Monitoring API.' % (response.status_code))

        return response

    def send(self, device_id, type, timestamp, data=None):
        """Performs the POST /device-event request on the API.

        The object passed to this function must have the following structure: ::

            {
              "device_id": 1,
              "remote_timestamp": 1566226133,
              "type": "chilemedido-error"
              "data": {...}
            }


        :param device_id: A device id number.
        :type device_id: integer
        :param type: The type of event string identifier.
        :type type: string
        :param data: A data JSON object.
        :type data: string
        :param timestamp: The timestamp of the event.
        :type timestamp: integer
        :param data: Additional data, defaults to None
        :type data: array, optional
        :return: The request response object.
        :rtype: class:`requests.Response`
        """
        payload = {
            'device_id': device_id,
            'type': type,
            'remote_timestamp': timestamp,
            'data': json.loads(data)
        }
        return self.handle_status(
            requests.post(self._url('/device-event/create'), headers=self._header(), json=payload))


if __name__ == '__main__':
    fire.Fire(MonitoringClient)
