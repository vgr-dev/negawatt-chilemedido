#!/bin/bash
# Copyright (C) 2019-2020 VGR SpA - All Rights Reserved
# Copyright (C) 2019-2020 Ingenieria y Energia NegaWatt Ltda. - All Rights Reserved
#
# Unauthorized copying of this file, via any medium is strictly prohibited.
# This code is propietary and confidential.
# You may not use this file except in compliance with the Copyright holders.
# Any kind of misuse of this file and intellectual properties may incur in legal prosecution.
#
# Author: Victor Gonzalez <victor@vgr.cl>

rm -rf dist/
rm -rf build/
pipenv install --dev
pipenv run pyinstaller fetch_measurement.py --onefile
pipenv run pyinstaller send_data.py --onefile
pipenv run pyinstaller send_historical_data.py --onefile
pipenv run pyinstaller monitoring.py --onefile
cp dist/fetch_measurement bundle
cp dist/send_data bundle
cp dist/send_historical_data bundle
cp dist/monitoring bundle
cp -R device_mapping bundle
cp example.config.yml bundle
cp historical_data.sh bundle
cp send_data.sh bundle