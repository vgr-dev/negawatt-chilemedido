# Copyright (C) 2019-2020 VGR SpA - All Rights Reserved
# Copyright (C) 2019-2020 Ingenieria y Energia NegaWatt Ltda. - All Rights Reserved
#
# Unauthorized copying of this file, via any medium is strictly prohibited.
# This code is proprietary and confidential.
# You may not use this file except in compliance with the Copyright holders.
# Any kind of misuse of this file and intellectual properties may incur in legal prosecution.
#
# Author: Victor Gonzalez <victor@vgr.cl>

"""Sends the last measured data to Chilemedido API.

This script will read data from the local database, and send the last measured data to Chilemedido.

If the data is correctly sent to Chilemedido, the data will be marked as sent,
if not, it will remain as not sent and will be picked up later by the ``send_historical_data.py`` process.

This will also log any relevant information during this process.
"""

import sys
import time

from src.api_client import ApiClient, ApiError, AuthenticationError
from src.config import load_config, save_config
from src.models import Measurement
from src.utils import data_to_chilemedido, chilemedido_error_event, data_sent_event, \
    heartbeat_event


def send_data():
    """Performs the script steps.
    """
    # load the config and setup the API client
    config = load_config()
    client = ApiClient(config['chilemedido']['access_token'], config['chilemedido']['refresh_token'])

    # handle authentication and API status
    try:
        client.get_sensor_name_list()
    except AuthenticationError:
        # credentials not valid, let's try to refresh them
        print('Obtaining new credentials...')
        print('Old refresh token: %s' % client.refresh_token)
        client.renew_credentials()
        print('New refresh token: %s' % client.refresh_token)
        config['chilemedido']['access_token'] = client.access_token
        config['chilemedido']['refresh_token'] = client.refresh_token
        save_config(config)
    except ApiError as err:
        chilemedido_error_event(config['devices'][0]['chilemedido_number'], {'message': str(err)})
        return err  # just exit if we have error trying to connect
    except Exception as err:
        chilemedido_error_event(config['devices'][0]['chilemedido_number'], {'message': str(err)})
        return err  # general error, just exit

    # fetch the last measurement for each device and send it
    for device_tag in config['devices']:
        chilemedido_number = config['devices'][device_tag]['chilemedido_number']
        query = Measurement \
            .select() \
            .where(Measurement.sent == False, Measurement.chilemedido_device_number == chilemedido_number) \
            .order_by(Measurement.local_timestamp.desc())
        heartbeat_event(chilemedido_number)

        try:
            measurement = query.get()  # shorthand for limit 1 and fetch row
            # send it to ChileMedido
            response = client.add_measurement(data_to_chilemedido(measurement))
            # if saved correctly, mark measurement as sent
            if response.json() and response.json()['success'] == True:
                measurement.sent = True
                measurement.save()
                print("Sent measure id %s for device number %s" % (measurement.id, chilemedido_number))

        except Measurement.DoesNotExist:
            print("No measurement for device number %s" % chilemedido_number)
            pass
        except AuthenticationError as err:
            chilemedido_error_event(chilemedido_number, {'message': str(err)})
            print(err)
            pass
        except ApiError as err:
            chilemedido_error_event(chilemedido_number, {'message': str(err)})
            print("ChileMedido API Error: %s" % err)
            pass
        except Exception as err:
            chilemedido_error_event(chilemedido_number, {'message': str(err)})
            print("Unknown error: %s" % err)
            pass
        else:
            data_sent_event(chilemedido_number)
            time.sleep(0.5)  # prevent rate limiting issues


if __name__ == "__main__":
    sys.exit(send_data())
