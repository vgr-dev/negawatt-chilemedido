import sys
import unittest
import src.register_convert as rc

FLOAT32_REG1 = 16457
FLOAT32_REG2 = 4059
FLOAT32_REAL = 3.1415927410125732

INT64_REG1 = 32767
INT64_REG2 = 65535
INT64_REG3 = 65535
INT64_REG4 = 65535
INT64_REAL = 9223372036854775807

UINT64_REG1 = 65535
UINT64_REG2 = 65535
UINT64_REG3 = 65535
UINT64_REG4 = 65535
UINT64_REAL = 18446744073709551615

class RegisterConversionTest(unittest.TestCase):
  def test_convert_registers_to_float32(self):
    self.assertEqual(rc.to_float32(FLOAT32_REG1, FLOAT32_REG2), FLOAT32_REAL)

  def test_convert_float32_to_registers(self):
    self.assertEqual(rc.from_float32(FLOAT32_REAL), (FLOAT32_REG1, FLOAT32_REG2))

  def test_convert_registers_to_int64(self):
    self.assertEqual(rc.to_int64(INT64_REG1, INT64_REG2, INT64_REG3, INT64_REG4), INT64_REAL)

  def test_convert_int64_to_registers(self):
    self.assertEqual(rc.from_int64(INT64_REAL), (INT64_REG1, INT64_REG2, INT64_REG3, INT64_REG4))

  def test_convert_registers_to_uint64(self):
    self.assertEqual(rc.to_uint64(UINT64_REG1, UINT64_REG2, UINT64_REG3, UINT64_REG4), UINT64_REAL)

  def test_convert_uint64_to_registers(self):
    self.assertEqual(rc.from_uint64(UINT64_REAL), (UINT64_REG1, UINT64_REG2, UINT64_REG3, UINT64_REG4))

if __name__ == "__main__":
  unittest.main()