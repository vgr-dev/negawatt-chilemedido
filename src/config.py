# Copyright (C) 2019-2020 VGR SpA - All Rights Reserved
# Copyright (C) 2019-2020 Ingenieria y Energia NegaWatt Ltda. - All Rights Reserved
#
# Unauthorized copying of this file, via any medium is strictly prohibited.
# This code is proprietary and confidential.
# You may not use this file except in compliance with the Copyright holders.
# Any kind of misuse of this file and intellectual properties may incur in legal prosecution.
#
# Author: Victor Gonzalez <victor@vgr.cl>
"""Collection of functions that read and write to the config file of the application.
"""

import yaml

config = None


def load_config():
    """Loads the config file ``config.yml`` **relative** to the caller script.

    :return: The config file data
    :rtype: array
    """
    global config
    with open('config.yml') as config_file:
        config = yaml.load(config_file, Loader=yaml.FullLoader)
        config_file.close()

    return config


def save_config(new_config):
    """Writes the data to the config file ``config.yml`` **relative** to the caller script.

    :param new_config: A dictionary with the  new configuration
    :type new_config: array
    :return: The new config file data
    :rtype: array
    """
    global config
    with open('config.yml', 'w') as config_file:
        config_file.write(yaml.dump(new_config))
        config_file.close()
        config = new_config

    return config
