# Copyright (C) 2019-2020 VGR SpA - All Rights Reserved
# Copyright (C) 2019-2020 Ingenieria y Energia NegaWatt Ltda. - All Rights Reserved
#
# Unauthorized copying of this file, via any medium is strictly prohibited.
# This code is proprietary and confidential.
# You may not use this file except in compliance with the Copyright holders.
# Any kind of misuse of this file and intellectual properties may incur in legal prosecution.
#
# Author: Victor Gonzalez <victor@vgr.cl>

"""ModbusReader is a class that performs reading operation on certain modbus TCP/IP devices.

You can read any register from any modbus TCP/IP device using big-endian byte orders.
When using this class you must provide the IP address of the modbus master device. Eg:

.. highlight:: python
.. code-block:: python

    my_reader = ModbusReader('127.0.0.1')

You can also configure a non-standard modbus port, the slave ID and register index offset.


This class can be used via command-line thanks to the `fire`_ package. Eg:

.. highlight:: bash
.. code-block:: bash

    python modbus_reader.py --address=127.0.0.1 --slave=3 measure_pm3250

For more information you can use the help flags `--help -h`

.. _fire: https://github.com/google/python-fire
"""

import json
import random

import fire
from pymodbus.client.sync import ModbusTcpClient

import src.register_convert as register_convert


class ModbusReader(object):
    """Reads data from TCP/IP modbus devices

    This function considers register offsets due to differences between documentation and hardware implementations.
    Usually the hardware is a zero-based array, but documentation is one-based array.

    :param address: The TCP/IP address of the master modbus device.
    :type address: string
    :param slave: The slave ID of the modbus device. Default is 1.
    :type slave: integer
    :param register_offset: The register address offset. Default is -1.
    :type register_offset: integer
    """

    def __init__(self, address, port=502, slave=1, register_offset=-1, verbose=False):
        self.address = address
        self.port = port
        self.client = ModbusTcpClient(self.address, self.port, timeout=1000)
        self.slave = slave
        self.register_offset = register_offset
        self.verboseprint = print if verbose else lambda *a, **k: None

    def read_float32(self, register):
        """Reads a float32 value from a modbus device using 2 contiguous registers

        This function receives the undermost register address for a float32 value separated in two contiguous registers.
        This function will use the register offset defined in the class constructor.

        :param register: Undermost register address for a value
        :type register: integer
        :return: The parsed value
        :rtype: float
        """
        response = self.client.read_holding_registers(register + self.register_offset, 2, unit=self.slave)
        return register_convert.to_float32(response.registers[0], response.registers[1])

    def read_int64(self, register):
        """Reads a signed int64 value from a modbus device using 4 contiguous registers

        This function receives the undermost register address for a signed int64 value separated in 4 contiguous registers.
        This function will use the register offset defined in the class constructor.

        :param register: Undermost register address for a value
        :type register: integer
        :return: The parsed value
        :rtype: integer
        """
        response = self.client.read_holding_registers(register + self.register_offset, 4, unit=self.slave)
        return register_convert.to_int64(response.registers[0], response.registers[1], response.registers[2],
                                         response.registers[3])

    def read_uint64(self, register):
        """Reads an unsigned int64 value from a modbus device using 4 contiguous registers

        This function receives the undermost register address for a unsigned int64 value separated in 4 contiguous
        registers. This function will use the register offset defined in the class constructor.

        :param register: Undermost register address for a value
        :type register: integer
        :return: The parsed value
        :rtype: integer
        """
        response = self.client.read_holding_registers(register + self.register_offset, 4, unit=self.slave)
        return register_convert.to_uint64(response.registers[0], response.registers[1], response.registers[2],
                                          response.registers[3])

    def fetch_variable_value(self, variable_config, custom_registers=None):
        """Fetches a variable from a modbus device and converts it to its configured value type.

        This function also accepts custom registers passed as array of registers, to facilitate the testing
        and mock-up of further functions.

        :param variable_config: A dictionary with the configuration of the variable
        :type variable_config: array
        :param custom_registers: An array of registers values (16-bit integers), defaults to None
        :type custom_registers: array, optional
        :return: A single value
        :rtype: integer, float
        """

        # prepare the useful variables
        value = None
        chilemedido_variable = variable_config['chilemedido_variable']
        register = variable_config['register']
        data_type = variable_config['type']
        prefix_correction = variable_config['prefix_correction']

        if not prefix_correction:
            prefix_correction = 1

        self.verboseprint('Parsing value "%s" in register %s as %s (prefix correction: %s)...' %
                          (chilemedido_variable, register, data_type, prefix_correction))

        # perform either read from modbus or a mock read
        if custom_registers:
            if variable_config['type'] == 'float32':
                value = register_convert.to_float32(custom_registers[0], custom_registers[1]) * prefix_correction
            elif variable_config['type'] == 'int64':
                value = int(register_convert.to_int64(custom_registers[0], custom_registers[1], custom_registers[2],
                                                      custom_registers[3]) * prefix_correction)
            elif variable_config['type'] == 'uint64':
                value = int(register_convert.to_uint64(custom_registers[0], custom_registers[1], custom_registers[2],
                                                       custom_registers[3]) * prefix_correction)
        else:
            if variable_config['type'] == 'float32':
                value = self.read_float32(register) * prefix_correction
            elif variable_config['type'] == 'int64':
                value = int(self.read_int64(register) * prefix_correction)
            elif variable_config['type'] == 'uint64':
                value = int(self.read_uint64(register) * prefix_correction)

        return value

    def measure_pm3250(self):
        """Extracts a measurement from a Schneider PM3250 modbus device.

        This function will read from the device mapping files in the project, and create a measurement according to those
        configurations.

        :return: A measurement dictionary
        :rtype: array
        """
        with open('device_mapping/pm3250.json') as json_file:
            device_mapping = json.load(json_file)
            measurement = dict()

            for variable_config in device_mapping:
                measurement[variable_config['chilemedido_variable']] = self.fetch_variable_value(variable_config)

            self.verboseprint(measurement)
            return measurement

    def measure_iem2150(self):
        """Extracts a measurement from a Schneider iEM2150 modbus device.

        This function will read from the device mapping files in the project, and create a measurement according to those
        configurations.

        :return: A measurement dictionary
        :rtype: array
        """
        with open('device_mapping/iem2150.json') as json_file:
            device_mapping = json.load(json_file)
            measurement = dict()

            for variable_config in device_mapping:
                measurement[variable_config['chilemedido_variable']] = self.fetch_variable_value(variable_config)

            self.verboseprint(measurement)
            return measurement

    def mock_measure_pm3250(self):
        """Mocks a measurement from a Schneider PM3250 modbus device.

        This function will mock a reading from the device mapping files in the project, and create a measurement
        according to those configurations.

        :return: A measurement dictionary
        :rtype: array
        """
        with open('device_mapping/pm3250.json') as json_file:
            device_mapping = json.load(json_file)
            measurement = dict()

            for variable_config in device_mapping:
                if variable_config['type'] == 'float32':
                    measurement[variable_config['chilemedido_variable']] = random.random()
                else:
                    measurement[variable_config['chilemedido_variable']] = random.randint(0, 300)

            self.verboseprint(measurement)
            return measurement

    def mock_measure_iem2150(self):
        """Mocks a measurement from a Schneider iEM2150 modbus device.

        This function will mock a reading from the device mapping files in the project, and create a measurement according to those configurations.

        :return: A measurement dictionary
        :rtype: array
        """
        with open('device_mapping/iem2150.json') as json_file:
            device_mapping = json.load(json_file)
            measurement = dict()

            for variable_config in device_mapping:
                if variable_config['type'] == 'float32':
                    measurement[variable_config['chilemedido_variable']] = random.random()
                else:
                    measurement[variable_config['chilemedido_variable']] = random.randint(0, 300)

            self.verboseprint(measurement)
            return measurement


if __name__ == '__main__':
    fire.Fire(ModbusReader)
