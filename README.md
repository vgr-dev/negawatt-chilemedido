<div align="center">
  <a href="" rel="noopener">
 <img width=200px height=200px src="docs/logo_transparent.png" alt="Project logo"></a>
</div>

<div align="center">

  [![Status](https://gitlab.com/vgr-dev/negawatt-chilemedido/badges/master/pipeline.svg)](https://gitlab.com/vgr-dev/negawatt-chilemedido/commits/master)
  [![Coverage](https://gitlab.com/vgr-dev/negawatt-chilemedido/badges/master/coverage.svg)](https://gitlab.com/vgr-dev/negawatt-chilemedido/commits/master)
  [![GitLabIssues](https://img.shields.io/badge/issues-Gitlab%20Issues-green)](https://gitlab.com/vgr-dev/negawatt-chilemedido/issues)
  [![License](https://img.shields.io/badge/license-propietary-red)](/LICENSE)

</div>

---

<div align="center"> A simple program made to fetch information from Modbus devices and send them to ChileMedido
    <br>
</div>

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## 📝 Table of Contents

- [🧐 About](#%F0%9F%A7%90-about)
- [🏁 Getting Started](#%F0%9F%8F%81-getting-started)
    - [Prerequisites](#prerequisites)
    - [Installing](#installing)
- [🔧 Running the tests](#%F0%9F%94%A7-running-the-tests)
- [🎈 Usage](#%F0%9F%8E%88-usage)
- [🚀 Deployment](#%F0%9F%9A%80-deployment)
- [⛏️ Built Using](#%E2%9B%8F%EF%B8%8F-built-using)
- [✍️ Authors](#%E2%9C%8D%EF%B8%8F-authors)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## 🧐 About
This project is made to be run within a Raspberry Pi, but it can run perfectly fine in any Linux system. It will read information from Modbus devices, and send the information to the ChileMedido API.

This specific project is just the source code, and it's not intended to be distributed, since binaries will be built for distribution.

## 🏁 Getting Started
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See [deployment](#deployment) for notes on how to deploy the project on a live system.

### Prerequisites
You need to have installed Python 3.5+ at least, pipenv, and some additional packages to handle the build of the GUI elements of this project:

```bash
# install system-wide dependencies
sudo apt install python3-dev python3-pip python3-psutil libgtk-3-dev libjpeg-dev sqlite3
# install pipenv globally
sudo pip3 install pipenv
```

### Installing
To setup your development or production-ready installation, you will need to use `pipenv`, which will take care of the dependencies and the virtual-environment, including the required `python` version to run the project.

Install the dependencies with `pipenv`:

```bash
pipenv install --dev
```

And that's it! Then you can run any script without the need to activate the virtual environment, using:

```bash
pipenv run python fetch_measurement.py
```

Or you can activate the shell, and run the commands from within the virtual environment:

```bash
pipenv shell
```

## 🔧 Running the tests

```bash
pipenv run python -m unittest discover
pipenv run python -m coverage run --source=src/ -m unittest discover
pipenv run python -m coverage report
```

## 🎈 Usage
Add notes about how to use the system.

## 🚀 Deployment
Add additional notes about how to deploy this on a live system.

## ⛏️ Built Using
- [SQLite](https://www.sqlite.org/) - Database
- [Python](https://www.python.org/) - Source code and testing
- [Gooey](https://github.com/chriskiehl/Gooey) - GUI Framework
- [Python Fire](https://github.com/google/python-fire) - CLI Framework
- [PyInstaller](https://www.pyinstaller.org/) - Binary executables
- [Visual Studio Code](https://code.visualstudio.com/) - Code editing

## ✍️ Authors
- [Victor Gonzalez](https://gitlab.com/XzAeRo) - Lead Developer
- [Lucas Mardones](https://gitlab.com/lucasMardones91) - Developer
