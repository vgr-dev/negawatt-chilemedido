import sys
import unittest
import src.register_convert as rc
from src.modbus_reader import ModbusReader

FLOAT32_REG1 = 16457
FLOAT32_REG2 = 4059
FLOAT32_REAL = 3.1415927410125732

INT64_REG1 = 32767
INT64_REG2 = 65535
INT64_REG3 = 65535
INT64_REG4 = 65535
INT64_REAL = 9223372036854775807

UINT64_REG1 = 65535
UINT64_REG2 = 65535
UINT64_REG3 = 65535
UINT64_REG4 = 65535
UINT64_REAL = 18446744073709551615

class ModbusReaderTest(unittest.TestCase):
  def setUp(self):
    self.reader = ModbusReader('127.0.0.1')
    self.float32_array = [FLOAT32_REG1, FLOAT32_REG2]
    self.int64_array = [INT64_REG1, INT64_REG2, INT64_REG3, INT64_REG4]
    self.uint64_array = [UINT64_REG1, UINT64_REG2, UINT64_REG3, UINT64_REG4]
    self.mock_float32_config = {
      'chilemedido_variable': 'Custom float32 Variable',
      'variable_name': 'This field will not be used',
      'register': 1234,
      'type': 'float32',
      'prefix_correction': ''
    }
    self.mock_int64_config = {
      'chilemedido_variable': 'Custom int64 Variable',
      'variable_name': 'This field will not be used',
      'register': 1234,
      'type': 'int64',
      'prefix_correction': 0.001
    }
    self.mock_uint64_config = {
      'chilemedido_variable': 'Custom uint64 Variable',
      'variable_name': 'This field will not be used',
      'register': 1234,
      'type': 'uint64',
      'prefix_correction': 1
    }

  def test_fetch_variable(self):
    self.assertEqual(FLOAT32_REAL, self.reader.fetch_variable_value(self.mock_float32_config, self.float32_array))
    self.assertEqual(int(INT64_REAL*0.001), self.reader.fetch_variable_value(self.mock_int64_config, self.int64_array))
    self.assertEqual(UINT64_REAL, self.reader.fetch_variable_value(self.mock_uint64_config, self.uint64_array))