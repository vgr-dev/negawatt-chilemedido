# Copyright (C) 2019-2020 VGR SpA - All Rights Reserved
# Copyright (C) 2019-2020 Ingenieria y Energia NegaWatt Ltda. - All Rights Reserved
#
# Unauthorized copying of this file, via any medium is strictly prohibited.
# This code is proprietary and confidential.
# You may not use this file except in compliance with the Copyright holders.
# Any kind of misuse of this file and intellectual properties may incur in legal prosecution.
#
# Author: Victor Gonzalez <victor@vgr.cl>

import struct

import fire


def to_float32(register1, register2):
    """Joins 2 registers of 2 bytes each, and converts them to a 32-bit floating point number.

    Assumes big-endian byte-order and that the registers are unsigned shorts (2 bytes / 16 bits).

    :param register1: 2 bytes unsigned integer
    :type register1: integer
    :param register2: 2 bytes unsigned integer
    :type register2: integer
    :return: A 32-bit floating point number
    :rtype: float
    """
    struct_value = struct.pack('>HH', register1, register2)
    return struct.unpack('>f', struct_value)[0]


def to_int64(register1, register2, register3, register4):
    """Joins 4 registers of 2 bytes each, and converts them to a signed 64-bit integer.

    Assumes big-endian byte-order and that the registers are unsigned shorts (2 bytes / 16 bits).

    :param register1: 2 bytes unsigned integer
    :type register1: integer
    :param register2: 2 bytes unsigned integer
    :type register2: integer
    :param register3: 2 bytes unsigned integer
    :type register3: integer
    :param register4: 2 bytes unsigned integer
    :type register4: integer
    :return: A 64-bit signed integer
    :rtype: integer
    """
    struct_value = struct.pack('>HHHH', register1, register2, register3, register4)
    return struct.unpack('>q', struct_value)[0]


def to_uint64(register1, register2, register3, register4):
    """Joins 4 registers of 2 bytes each, and converts them to an unsigned 64-bit integer.

    Assumes big-endian byte-order and that the registers are unsigned shorts (2 bytes / 16 bits).

    :param register1: 2 bytes unsigned integer
    :type register1: integer
    :param register2: 2 bytes unsigned integer
    :type register2: integer
    :param register3: 2 bytes unsigned integer
    :type register3: integer
    :param register4: 2 bytes unsigned integer
    :type register4: integer
    :return: A 64-bit unsigned integer
    :rtype: integer
    """
    struct_value = struct.pack('>HHHH', register1, register2, register3, register4)
    return struct.unpack('>Q', struct_value)[0]


def from_float32(value):
    """Converts a float32 value into 2 big-endian 2 byte integers.

    :param value: The value to be converted. Eg: 3.1415684
    :type value: float
    :return: A tuple with two integers.
    :rtype: tuple
    """
    struct_value = struct.pack('>f', value)
    return struct.unpack('>HH', struct_value)


def from_int64(value):
    """Converts a signed int64 value into 4 big-endian 2 byte integers.

    :param value: The value to be converted. Eg: 132351
    :type value: integer
    :return: A tuple with four integers.
    :rtype: tuple
    """
    struct_value = struct.pack('>q', value)
    return struct.unpack('>HHHH', struct_value)


def from_uint64(value):
    """Converts an unsigned int64 value (uint64) into 4 big-endian 2 byte integers.

    :param value: The value to be converted. Eg: -132351
    :type value: integer
    :return: A tuple with four integers.
    :rtype: tuple
    """
    struct_value = struct.pack('>Q', value)
    return struct.unpack('>HHHH', struct_value)


if __name__ == '__main__':
    fire.Fire()
